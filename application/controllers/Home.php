<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        error_reporting(E_ALL);
        ini_set("display_errors", "1");
        $this->load->database();
        $this->load->model('frontend/Home_model', 'home');
        _site_setting();
    }

    public function index()
    {
        $page_data['active_tab'] = 'home';
        $page_data['page'] = 'home';
        $page_data['recent_articles'] = $this->home->get_articles();
        $this->load->view('frontend/template', $page_data);
    }

    public function view($slug)
    {
        $page_data['active_tab'] = 'home';
        $page_data['page'] = 'article_view';
        $page_data['article'] = $this->home->get_article_details($slug);
        $this->load->view('frontend/template', $page_data);
    }
}
