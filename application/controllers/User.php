<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('frontend/User_model', 'user');
        $this->load->model('frontend/Home_model', 'home');
    }

    public function login() {
        if (_session('_logged_in') == 'yes') {
            $redirect = _session('redirect');
            if ($redirect) {
                $this->session->unset_userdata(['redirect' => '']);
                redirect($redirect);
            } else {
                redirect('home');
            }
        }
        $page_data['error'] = '';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'email/username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
            if ($this->form_validation->run() == TRUE) {
                $user = $this->user->login($this->input->post('username'), md5($this->input->post('password')));
                if (!empty($user)) {
                    $session = array(
                        '_fullname' => ucfirst($user->first_name) . ' ' . ucfirst($user->last_name),
                        '_email' => $user->email,
                        '_user_id' => $user->user_id,
                        '_logged_in' => 'yes'
                    );

                    $this->session->set_userdata($session);
                    $redirect = _session('redirect');
                    if ($redirect) {
                        $this->session->unset_userdata(['redirect' => '']);
                        redirect($redirect);
                    } else {
                        redirect('home');
                    }
                } else {
                    $page_data['error'] = 'Invalid login credentials';
                    $page_data['page'] = 'login_register';
                    $this->load->view('frontend/template', $page_data);
                }
            } else {                
                $page_data['page'] = 'login_register';
                $this->load->view('frontend/template', $page_data);
            }
        } else {
            if (isset($_GET['redirect'])) {
                if ($_GET['redirect'] != '') {
                    $this->session->set_userdata('redirect', $_GET['redirect']);
                }
            }
            $page_data['page'] = 'login_register';
            $this->load->view('frontend/template', $page_data);
        }
    }

    public function change_password() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('old', 'Username', 'required');
            $this->form_validation->set_rules('password1', 'Password', 'required');
            $this->form_validation->set_rules('password2', 'Confirm Password', 'required|matches[password1]');
            if ($this->form_validation->run() == TRUE) {
                $pass1 = $this->input->post('password1');
                if ($pass1 != $this->input->post('password2')) {
                    echo json_encode(array('status' => 'not_matching'));
                } else {
                    $user = $this->user->login(false, md5($this->input->post('old')), _session('admin_id'));
                    if (!empty($user)) {
                        $this->user->update_user(_session('admin_id'), array('password' => md5($pass1)));
                        echo json_encode(array('status' => 'done'));
                    } else {
                        echo json_encode(array('status' => 'invalid'));
                    }
                }
            } else {
                $page_data['page'] = 'login_register';
                $this->load->view('frontend/template', $page_data);
            }
        } else {
            $page_data['page'] = 'login_register';
            $this->load->view('frontend/template', $page_data);
        }
        exit;
    }

    public function register() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_message('is_unique', 'The %s is already taken');
            $this->form_validation->set_rules('first_name', 'first name', 'required');
            $this->form_validation->set_rules('last_name', 'last name', 'required');
            $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('contact_number', 'mobile number', 'required|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('password1', 'password', 'required');
            $this->form_validation->set_rules('password2', 'confirm password', 'required|matches[password1]');
            if ($this->form_validation->run() == TRUE) {
                $pass1 = $this->input->post('password1');
                if ($pass1 != $this->input->post('password2')) {
                    $page_data['error'] = 'Both passord are not matching';
                    
                } else {
                    $user = [
                        'first_name' => ucfirst($this->input->post('first_name')),
                        'last_name' => ucfirst($this->input->post('last_name')),
                        'email' => $this->input->post('email'),
                        'contact_number' => $this->input->post('contact_number'),
                        'password' => md5($pass1)
                    ];
                    $status = $this->user->register($user);
                    if ($status) {
                        $this->session->set_flashdata('_message', 'Registration successfull. Now try login');
                    } else {
                        $this->session->set_flashdata('_error', 'Registration failed. Invalid data provided');
                    }
                }
                $page_data['page'] = 'login_register';
                $this->load->view('frontend/template', $page_data);
            } else {
                $page_data['page'] = 'login_register';
                $this->load->view('frontend/template', $page_data);
            }
        } else {
            $page_data['page'] = 'login_register';
            $this->load->view('frontend/template', $page_data);
        }
    }

    public function logout() {
        $cart = _session('cart');
        $this->session->sess_destroy();
        $this->session->set_userdata('cart', $cart);
        redirect('home');
    }

}
