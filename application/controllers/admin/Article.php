<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Article extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('admin/Article_model', 'article');
        if (_session('logged_in') != 'yes') {
            redirect('admin/login');
        }
    }

    public function index()
    {
        $page_data['page'] = 'article';
        $page_data['tab'] = 'article';
        $this->load->view('admin/template', $page_data);
    }

    public function save($id = false)
    {
        $page_data['edit_id'] = $id;
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('category_id', 'Category', 'required');
            $this->form_validation->set_rules('article_title', 'Article title', 'required');
            $this->form_validation->set_rules('article_body', 'Article body', 'required');

            if ($this->form_validation->run() == true) {
                if (count($_FILES) > 0) {
                    if ($_FILES['image']['name'] != '') {
                        $uploaded = $this->upload_article_file(ARTICLE_UPLOAD_PATH, $_FILES);
                    }
                } else {
                    $uploaded = '';
                }

                if ($id) {
                    $data = array(
                        'article_title' => ucfirst($this->input->post('article_title')),
                        'article_body' => $this->input->post('article_body'),
                        'category_id' => $this->input->post('category_id'),
                        'created_by' => _session('admin_id'),
                        'updated_on' => _date(),
                    );
                    if ($uploaded) {
                        $data['article_image'] = $uploaded;
                    }
                    $this->article->save($data, $id);
                    $this->session->set_flashdata('msg', 'Article Updated Successfully');
                } else {
                    $data = array(
                        'category_id' => $this->input->post('category_id'),
                        'article_title' => ucfirst($this->input->post('article_title')),
                        'article_slug' => generate_url_slug($this->input->post('article_title')) . '-' . strtotime(_date()),
                        'article_body' => $this->input->post('article_body'),
                        'article_image' => $uploaded,
                        'created_by' => _session('admin_id'),
                        'updated_on' => _date(),
                    );
                    $article_id = $this->article->save($data);
                    $this->session->set_flashdata('msg', 'Article Added Successfully');
                }

                $this->session->set_flashdata('msg', 'Success : Article saved successfully');
                redirect('admin/article');
            } else {
                if ($id) {
                    $page_data['edit_id'] = $id;
                    $article_details = $this->article->get_article($id);
                    if (!empty($article_details)) {

                    } else {
                        $this->session->set_flashdata('msg', 'Error : Article not found');
                        redirect('admin/articles');
                    }
                }

                $page_data['all_categories'] = $this->article->get_all_categories();
                $page_data['page'] = 'article_save';
                $page_data['tab'] = 'article';
                $this->load->view('admin/template', $page_data);
            }
        } else {
            if ($id) {
                $page_data['edit_id'] = $id;
                $article_details = $this->article->get_article($id);
                if (!empty($article_details)) {
                    $page_data['article_details'] = $article_details;
                } else {
                    $this->session->set_flashdata('msg', 'Error : Article not found');
                    redirect('admin/articles');
                }
            }

            $page_data['all_categories'] = $this->article->get_all_categories();
            $page_data['page'] = 'article_save';
            $page_data['tab'] = 'article';

            $this->load->view('admin/template', $page_data);
        }
    }

    public function view($id)
    {
        if (!$id) {
            redirect('admin/article');
        }
        $page_data['article_details'] = $this->article->get_article_view($id);

        if (!empty($page_data['article_details'])) {
            $page_data['article_attributes'] = $this->article->get_article_attributes_view($id);
            $page_data['article_images'] = $this->article->get_article_images($id);
            $page_data['article_sizes'] = $this->article->get_article_sizes($id, false);
            $page_data['article_colors'] = $this->article->get_article_colors($id, false);
        } else {
            redirect('admin/article');
        }
        $page_data['page'] = 'article_view';
        $page_data['tab'] = 'article';

        $this->load->view('admin/template', $page_data);
    }

    public function delete($id)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->article->delete($id);
        }
        $this->session->set_flashdata('msg', 'Article Deleted Successfully');
        redirect('admin/article');
    }

    public function paginate()
    {
        if (isset($_GET['draw'])) {
            $draw = $_GET['draw'];
        } else {
            $draw = 1;
        }
        if (isset($_GET['start'])) {
            $offset = $_GET['start'];
        } else {
            $offset = 0;
        }
        if (isset($_GET['length'])) {
            $limit = $_GET['length'];
        } else {
            $limit = DEFAULT_LIMIT;
        }
        if (isset($_GET['search']['value'])) {
            $search = $_GET['search']['value'];
        } else {
            $search = "";
        }
        if (isset($_GET['order']['0']['dir'])) {
            $sort = $_GET['order']['0']['dir'];
        } else {
            $sort = "asc";
        }
        $recordsFiltered = $recordsTotal = $this->article->listing(true, false, false, $search);
        $articles = $this->article->listing(false, $offset, $limit, $search, $sort);
        $data = [];
        $return = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => []
        );
        if ($recordsTotal > 0) {
            $i = 0;
            foreach ($articles as $article) {
                $i++;
                $confirm_delete = "confirm_delete('" . base_url('admin/article/delete/' . $article->article_id) . "','Are you sure want to delete ?')";
                $edit_anchor = '<a href="' . base_url('admin/article/save/' . $article->article_id) . '" class="btn btn-info btn-flat btn-sm btn-margin-left"><i class="fa fa-pencil"></i> <span>Edit</span>';
                $view_anchor = '<a target="_blank" href="' . base_url('admin/article/save/' . $article->article_id) . '" class="btn btn-success btn-flat btn-sm btn-margin-left"><i class="fa fa-eye"></i> <span>View</span>';
                $delete_anchor = '</a><button onclick="' . $confirm_delete . '" class="btn btn-danger btn-flat btn-sm btn-margin-left"><i class="fa fa-times"></i> <span>Delete</span></button>';
                $action = $view_anchor . $edit_anchor . $delete_anchor;
                if ($article->article_image) {
                    $image = get_article_image($article->article_image);
                } else {
                    $image = '';
                }

                if ($article->status) {
                    $status = '<span>Published</span><br><br><a href="javascript:void(0)"  class="btn btn-warning btn-flat btn-sm" onclick="changeArticleStatus(this,' . $article->article_id . ',0)">Make Draft</a>';
                } else {
                    $status = '<span>Draft</span><br><br><a href="javascript:void(0)" class="btn btn-warning btn-flat btn-sm" onclick="changeArticleStatus(this,' . $article->article_id . ',1)">Publish Now</a>';
                }

                $return['data'][] = array(
                    $i,
                    $image,
                    $article->category_name,
                    truncate_string($article->article_title, 40),
                    truncate_string($article->article_body, 100),
                    $status,
                    $action);
            }
        }
        echo json_encode($return);
    }

    private function upload_article_file($path, $files)
    {
        if (!file_exists($path)) {
            $mask = umask(0);
            mkdir($path, 0777, true);
            umask($mask);
        }
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|gif|png|jpeg',
            'overwrite' => 1,
        );

        $this->load->library('upload', $config);
        $images = array();
        $ext = get_file_extension($files['image']['name']);
        $fileName = 'article_' . time() . '.' . $ext;
        $config['file_name'] = $fileName;
        $this->upload->initialize($config);
        if ($this->upload->do_upload('image')) {
            return $fileName;
        } else {
            return false;
        }
    }

    public function image_delete($id)
    {
        if ($id) {
            $this->article->delete_article_image($id, true);
        }
        echo json_encode(['status' => true]);
    }

    public function change_status()
    {
        $return = array('status' => 0);
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $article_id = $this->input->post('article_id');
            $status = $this->input->post('status');
            $statusFlag = $this->article->change_article_status($article_id, $status);
            if ($statusFlag) {
                if ($status == 1) {
                    $html = '<span>Published</span><br><br><a href="javascript:void(0)"  class="btn btn-warning btn-flat btn-sm" onclick="changeArticleStatus(this,' . $article_id . ',0)">Make Draft</a>';
                } else {
                    $html = '<span>Draft</span><br><br><a href="javascript:void(0)" class="btn btn-warning btn-flat btn-sm" onclick="changeArticleStatus(this,' . $article_id . ',1)">Publish Now</a>';
                }
                $return = array(
                    'status' => 1,
                    'html' => $html,
                );
            }
        }
        echo json_encode($return);
        exit;
    }

}
