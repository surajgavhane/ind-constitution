<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
//        error_reporting(E_ALL);
//        ini_set("display_errors", "1");
        $this->load->database();
        $this->load->model('admin/dashboard_model', 'dashboard');
        if (_session('logged_in') != 'yes') {
            redirect('admin/login');
        }
    }

    public function index() {
        $page_data['total_products'] = 1; //$this->dashboard->get_products_total();
        $page_data['total_customers'] = 0;
        $page_data['total_orders'] = 0;
        $page_data['total_sale'] = 0;
        $page_data['recent_orders'] = 0;
        $page_data['page'] = 'dashboard';
        $page_data['tab'] = 'dashboard';
        $this->load->view('admin/template', $page_data);
    }

}
