<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
//        error_reporting(E_ALL);
//        ini_set("display_errors", "1");
        $this->load->database();
        $this->load->model('admin/Crud_model', 'crud');
        $this->load->model('admin/Setting_model', 'setting');
    }

    public function index() {
        if (_session('logged_in') == 'yes') {
            redirect('admin/dashboard');
        }
        $page_data['error'] = '';
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            if ($this->form_validation->run() == TRUE) {
                $user = $this->crud->login($this->input->post('username'), md5($this->input->post('password')));
                if (!empty($user)) {
                    $session = array(
                        'fullname' => ucfirst($user->firstname) . ' ' . ucfirst($user->surname),
                        'email' => $user->email,
                        'admin_id' => $user->user_id,
                        'logged_in' => 'yes'
                    );

                    $setting = $this->setting->get_site_settings();
                    $session['site_name'] = $setting['site_name'];
                    $session['site_copyright'] = $setting['site_copyright'];
                    $session['site_logo'] = $setting['site_logo'];
                    $this->session->set_userdata($session);
                    redirect('admin/dashboard');
                } else {
                    $page_data['error'] = 'Invalid login credentials';
                    $this->load->view('admin/login', $page_data);
                }
            } else {
                $this->load->view('admin/login', $page_data);
            }
        } else {
            $this->load->view('admin/login', $page_data);
        }
    }

    public function change_password() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('old', 'Username', 'required');
            $this->form_validation->set_rules('password1', 'Password', 'required');
            $this->form_validation->set_rules('password2', 'Password', 'required');
            if ($this->form_validation->run() == TRUE) {
                $pass1 = $this->input->post('password1');
                if ($pass1 != $this->input->post('password2')) {
                    echo json_encode(array('status' => 'not_matching'));
                } else {
                    $user = $this->crud->login(false, md5($this->input->post('old')), _session('admin_id'));
                    if (!empty($user)) {
                        $this->crud->update_user(_session('admin_id'), array('password' => md5($pass1)));
                        echo json_encode(array('status' => 'done'));
                    } else {
                        echo json_encode(array('status' => 'invalid'));
                    }
                }
            } else {
                echo json_encode(array('status' => 'validate'));
            }
        } else {
            echo json_encode(array('status' => 'validate'));
        }
        exit;
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('admin/login');
    }

}
