<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('admin/Setting_model', 'setting');
        if (_session('logged_in') != 'yes') {
            redirect('admin/login');
        }
    }

    public function site() {
        $page_data['page'] = 'site_setting';
        $page_data['tab'] = 'site_setting';
        $this->load->view('admin/template', $page_data);
    }

    public function site_save() {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('setting_value', 'Setting', 'required');
            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'setting_value' => $this->input->post('setting_value')
                );
                $id = $this->input->post('edit_id');
                if ($id) {
                    $status = $this->setting->site_setting_save($data, $id);
                    if ($status) {
                        $this->session->set_flashdata('msg', 'Setting Updated Successfully');
                    } else {
                        $this->session->set_flashdata('msg', 'Error: Setting Updated failed');
                    }
                }
                $setting = $this->setting->get_site_settings();
                $this->session->set_userdata('site_name', $setting['site_name']);
                $this->session->set_userdata('site_copyright', $setting['site_copyright']);
                $this->session->set_userdata('site_logo', $setting['site_logo']);
                redirect('admin/setting/site');
            } else {
                $this->session->set_flashdata('msg', 'Error: Invalid setting value provided');
                redirect('admin/setting/site');
            }
        }
        redirect('admin/setting/site');
    }

    public function site_paginate() {
        if (isset($_GET['draw'])) {
            $draw = $_GET['draw'];
        } else {
            $draw = 1;
        }
        if (isset($_GET['start'])) {
            $offset = $_GET['start'];
        } else {
            $offset = 0;
        }
        if (isset($_GET['length'])) {
            $limit = $_GET['length'];
        } else {
            $limit = DEFAULT_LIMIT;
        }
        if (isset($_GET['search']['value'])) {
            $search = $_GET['search']['value'];
        } else {
            $search = "";
        }
        if (isset($_GET['order']['0']['dir'])) {
            $sort = $_GET['order']['0']['dir'];
        } else {
            $sort = "asc";
        }
        $recordsFiltered = $recordsTotal = $this->setting->site_setting_listing(true, false, false, $search);
        $sites = $this->setting->site_setting_listing(false, $offset, $limit, $search, $sort);
        $data = [];
        $return = array(
            'draw' => $draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => []
        );
        if ($recordsTotal > 0) {
            $i = 0;
            foreach ($sites as $site) {
                $i++;
                if ($site->setting_key == 'site_logo') {
                    $action = '<button data-sid="' . $site->setting_id . '" data-sname="' . $site->setting_name . '" data-sval="' . get_site_logo($site->setting_value, true) . '" class="btn btn-info btn-flat btn-sm setting-logo-open" data-toggle="modal" dat-target="#setting_modal_logo"><i class="fa fa-pencil"></i> <span>Change</span></a>';
                    $setting_value = get_site_logo($site->setting_value);
                } else {
                    $action = '<button data-sid="' . $site->setting_id . '" data-sname="' . $site->setting_name . '" data-sval="' . $site->setting_value . '" class="btn btn-info btn-flat btn-sm setting-edit-open" data-toggle="modal" dat-target="#setting_modal_add"><i class="fa fa-pencil"></i> <span>Edit</span></a>';
                    $setting_value = $site->setting_value;
                }
                $return['data'][] = array($i, $site->setting_name, $setting_value, $action);
            }
        }
        echo json_encode($return);
    }

}
