<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('pr')) {

    function pr($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }

}
if (!function_exists('pre')) {

    function pre($array) {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;
    }

}

if (!function_exists('get_active_class')) {

    function get_active_class($para1, $para2) {
        if ($para1 == $para2) {
            return 'active ';
        } else {
            return ' ';
        }
    }

}

if (!function_exists('get_menu_class')) {

    function get_menu_class($para1, $para2) {
        if ($para1 == $para2) {
            return 'menu-open ';
        } else {
            return ' ';
        }
    }

}

if (!function_exists('convert_date')) {

    function convert_date($date, $format = false) {
        if ($format) {
            return date($format, strtotime($date));
        } else {
            return date('dS M, Y', strtotime($date));
        }
    }

}

if (!function_exists('_date')) {

    function _date() {
        return date('Y-m-d H:i:s');
    }

}

if (!function_exists('_session')) {

    function _session($key = false) {
        $CI = & get_instance();
        if ($key) {
            return $CI->session->userdata($key);
        } else {
            return $CI->session->All_userdata();
        }
    }

}

if (!function_exists('truncate_string')) {

    function truncate_string($string, $len = 70) {
        if (strlen($string) > $len) {
            return substr($string, 0, $len) . '...';
        } else {
            return $string;
        }
    }

}

if (!function_exists('get_file_extension')) {

    function get_file_extension($filename) {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        return $ext;
    }

}

if (!function_exists('get_article_image')) {

    function get_article_image($filename) {
        if (file_exists(ARTICLE_UPLOAD_PATH. '/' . $filename)) {
            $image_url = base_url() . ARTICLE_UPLOAD_SUBPATH  . $filename;
            $image = '<img src="' . $image_url . '" alt="Not Found" class="admin_prouduct_list_image" />';
            return $image;
        } else {
            return default_product_image();
        }
    }

}
if (!function_exists('get_article_url')) {

    function get_article_url($filename) {
        if (file_exists(ARTICLE_UPLOAD_PATH. '/' . $filename)) {
            return base_url() . ARTICLE_UPLOAD_SUBPATH  . $filename;
        } else {
            return base_url() . 'uploads/logos/no-available.png';
        }
    }

}

if (!function_exists('default_product_image')) {

    function default_product_image() {
        $image = '<img src="' . base_url() . 'images/default/product-image-default.gif' . '" alt="Not Found" class="admin_prouduct_list_image" />';
    }

}
if (!function_exists('get_site_logo')) {

    function get_site_logo($logo_name, $flag = false) {
        $url = base_url() . 'uploads/logos/' . $logo_name;
        if ($flag) {
            return $url;
        }
        return '<img src="' . $url . '" alt="Not Found" style="height: auto;width: 100%;max-width: 150px;" />';
    }

}
if (!function_exists('currency')) {

    function currency() {
        return '₹';
    }

}

if (!function_exists('_site_setting')) {

    function _site_setting() {
        $session = [];
        $CI = & get_instance();
        if (!$CI->session->userdata('_site_name')) {
            $setting = [];
            $result = $CI->db->get("site_setting")->result();
            foreach ($result as $row) {
                if ($row->setting_key == 'site_logo') {
                    $setting[$row->setting_key] = get_site_logo($row->setting_value, true);
                } else {
                    $setting[$row->setting_key] = $row->setting_value;
                }
            }
            $session['_site_name'] = $setting['site_name'];
            $session['_site_copyright'] = $setting['site_copyright'];
            $session['_site_logo'] = $setting['site_logo'];
            $CI->session->set_userdata($session);
        }
    }

}

if (!function_exists('get_active_tab')) {

    function get_active_tab($para1, $para2) {
        if ($para1 === $para2) {
            return 'active';
        } else {
            return ' ';
        }
    }

}

if (!function_exists('_input')) {

    function _input($key) {
        $CI = & get_instance();
        $input = $CI->input->get($key, true);
        return $input;
    }

}


if (!function_exists('generate_url_slug')) {

    function generate_url_slug($string) {
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', trim($string))));
        return $slug;
    }

}

if (!function_exists('total_pages')) {

    function total_pages($count) {
        $pages = $count / PRODUCT_LIMIT;
        if ($count % PRODUCT_LIMIT > 0) {
            $pages + 1;
        }
        return $pages;
    }

}

if (!function_exists('create_pagination')) {

    function create_pagination($total_pages, $page_offset, $total_count, $base) {
        if (strpos($base, '?') === false) {
            $base .= '?page=1';
        }

        $pagination = '<nav aria-label="navigation">
                    <ul class="pagination mt-50 mb-70">';

        if ($total_count == 0) {
            $pagination .= "<li class='page-item'><a class='page-link' href='javascript:void(0);' style='width: 200px;border: none;    cursor: default;;'>0 Results for Your Search</a></li>";
        } else {
            $record_start = $page_offset + 1;
            $record_count = $record_start + PRODUCT_LIMIT - 1;
            if ($record_count > $total_count) {
                $record_count = $total_count;
            }

            if ($total_pages > PAGINATION_NUMBER_LIMIT) {
                $prev_page_num = $page_offset;
                if ($prev_page_num >= 1) {
                    $pagination .= '<li class="page-item"><a class="page-link" href="' . $base . '&page=1">First</a></li>';
                    $pagination .= '<li class="page-item"><a class="page-link" href="' . $base . '&page=' . $prev_page_num . '"><i class="fa fa-angle-left"></i></a></li>';
                }
            }

            if ($page_offset != 0 && ($total_pages > (PAGINATION_NUMBER_LIMIT - 1))) {
                if ($page_offset >= 1) {
                    $i_start_index = $page_offset - 2;
                }
                if ($i_start_index < 0) {
                    $i_start_index = 0;
                }
                if (($total_pages - $i_start_index) < PAGINATION_NUMBER_LIMIT) {
                    $i_start_index = $total_pages - PAGINATION_NUMBER_LIMIT;
                }
                $loop_limit = PAGINATION_NUMBER_LIMIT + $i_start_index;
            } else {
                $i_start_index = 0;
                if ($total_pages < PAGINATION_NUMBER_LIMIT) {
                    $loop_limit = $total_pages;
                } else {
                    $loop_limit = PAGINATION_NUMBER_LIMIT + $i_start_index;
                }
            }

            for ($i = $i_start_index; $i < $loop_limit; $i++) {


                $page_num = $i * PRODUCT_LIMIT;
                $page_name = $i + 1;
                $pagination .= '<li class="page-item"><a class="page-link ';
                if ($page_num == $page_offset) {
                    $pagination .= ' active';
                    $_url = 'javascript:void(0);';
                } else {
                    $_url = $base . '&page=' . $page_name;
                }
                $pagination .= '" href="' . $_url . '">' . $page_name . '</a></li>';
            }
            if ($total_pages > PAGINATION_NUMBER_LIMIT) {
                $next_page_num = $page_offset + PRODUCT_LIMIT;
                if ($total_count > $next_page_num) {
                    $last_page_num = ($total_pages - 1) * PRODUCT_LIMIT;
                    $pagination .= '<li class="page-item"><a class="page-link" href="' . $base . '&page=' . $next_page_num . '"><i class="fa fa-angle-right"></i></a></li>';
                    if ($total_pages > $last_page_num) {
                        $pagination .= '<li class="page-item"><a class="page-link" href="' . $base . '&page=' . $last_page_num . '">Last</a></li>';
                    }
                }
            }
        }
        if ($total_pages > 0) {
            $pagination .= "<li class='page-item'><a class='page-link' href='javascript:void(0);' style='width: 200px;border: none;'>Showing " . $record_start . "-" . $record_count . " of " . $total_count . " results</a></li>";
        }
        $pagination .= '</ul></nav>';

        return $pagination;
    }

}
if (!function_exists('check_in_cart')) {

    function check_in_cart($product_id) {
        $CI = & get_instance();
        $cart = $CI->session->userdata('cart');
        if (!empty($cart)) {
            foreach ($cart as $c) {
                if ($c['product_id'] == $product_id) {
                    return true;
                }
            }
        }
        return false;
    }

}
?>

