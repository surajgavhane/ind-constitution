<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Article_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listing($count = false, $offset = 0, $limit = false, $search = false, $sort = false)
    {
        $sort = 'DESC';
        if ($count) {
            $this->db->select('COUNT(articles.article_id) as cnt');
        } else {
            $fields = 'articles.*,article_categories.category_name';
            $this->db->select($fields);
            $this->db->join('article_categories', 'article_categories.category_id= articles.category_id', 'left');
        }
        if (!empty($search)) {
            $this->db->like('articles.article_title', $search);
        }
        if ($count) {
            return $this->db->get('articles')->row()->cnt;
        } else {
            $this->db->order_by('articles.updated_on', $sort);
            $this->db->group_by('articles.article_id');
            $this->db->limit($limit, $offset);
            $res = $this->db->get('articles')->result();
            
            if (!empty($res)) {
                return $res;
            } else {
                return [];
            }
        }
    }

    public function get_article($id)
    {
        $this->db->where('article_id', $id);
        $row = $this->db->get('articles')->row();
        if (!empty($row)) {
            return $row;
        } else {
            return [];
        }
    }

    public function get_article_view($id)
    {
        $this->db->select('articles.*,c.category_name');
        $this->db->from('articles as p');
        $this->db->join('article_categories as c', 'c.category_id = articles.category_id');
        $this->db->where('articles.article_id', $id);
        $this->db->group_by('articles.article_id');
        $row = $this->db->get()->row();
        if (!empty($row)) {
            return $row;
        } else {
            return [];
        }
    }

    public function save($data, $id = false)
    {
        if ($id) {
            $this->db->where('article_id', $id);
            $this->db->update('articles', $data);
            return $id;
        } else {
            $this->db->insert('articles', $data);
            return $this->db->insert_id();
        }
    }

    public function delete($id)
    {
        $this->db->where('article_id', $id);
        $this->db->delete('articles');
        return true;
    }

    public function get_all_categories()
    {
        $this->db->select('*');
        $this->db->from('article_categories');
        $result = $this->db->get()->result();
        return $result;
    }

    public function change_article_status($article_id, $status)
    {
        $this->db->where('article_id', $article_id);
        $this->db->update('articles', array('status' => $status));
        return $this->db->affected_rows();
    }

}
