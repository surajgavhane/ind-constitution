<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function login($username, $pass, $id = false) {
        if ($id) {
            $this->db->where('user_id', $id);
        } else {
            $this->db->where('email', $username);
        }

        $this->db->where('password', $pass);
        $this->db->where('usertype', '1');
        $res = $this->db->get('admin_users')->result();
        if (!empty($res)) {
            return $res[0];
        }
    }

    function update_user($id, $data) {
        $this->db->where('user_id', $id);
        $this->db->update('admin_users', $data);
    }

}
