<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_products_total() {
        $this->db->select('COUNT(product_id) as CNT');
        return $this->db->get('products')->row()->CNT;
    }

    

}
