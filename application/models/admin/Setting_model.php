<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Setting_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function site_setting_listing($count = false, $offset = 0, $limit = false, $search = false, $sort = false) {
        if ($count) {
            $this->db->select("COUNT(setting_id) as cnt");
            $this->db->from("site_setting");
        } else {
            $this->db->select("*");
            $this->db->from("site_setting");
        }
        if (!empty($search)) {
            $this->db->like("setting_name", $search);
            $this->db->or_like("setting_value", $search);
        }
        if ($count) {
            return $this->db->get()->row()->cnt;
        } else {
            if ($sort) {
                $this->db->order_by("setting_name", $sort);
            }
            $this->db->limit($limit, $offset);
            $res = $this->db->get()->result();
            if (!empty($res)) {
                return $res;
            } else {
                return [];
            }
        }
    }

    function get_site_setting($id) {
        $this->db->where("setting_id", $id);
        $row = $this->db->get("site_setting")->row();
        if (!empty($row)) {
            return $row;
        } else {
            return [];
        }
    }

    function get_site_settings() {
        $return = [];
        $result = $this->db->get("site_setting")->result();
        foreach ($result as $row) {
            if ($row->setting_key == 'site_logo') {
                $return[$row->setting_key] = get_site_logo($row->setting_value, true);
            } else {
                $return[$row->setting_key] = $row->setting_value;
            }
        }
        return $return;
    }

    function site_setting_save($data, $id = false) {
        if ($id) {
            $this->db->where("setting_id", $id);
            $this->db->update("site_setting", $data);
            return true;
        }
        return true;
    }

}
