<?php

if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Home_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function get_header_categories() {
        $this->db->where("status", "1");
        $this->db->where("show_status", "1");
        $result = $this->db->get("categories")->result();
        return $result;
    }

    function get_articles($count=false,$search='') {
        $sort = 'DESC';
        $offset = 0;
        if ($count) {
            $this->db->select('COUNT(articles.article_id) as cnt');
        } else {
            $fields = 'articles.*';
            $this->db->select($fields);            
        }
        if (!empty($search)) {
            $this->db->like('articles.article_title', $search);
        }
        if ($count) {
            return $this->db->get('articles')->row()->cnt;
        } else {
            $this->db->order_by('articles.updated_on', $sort);
            $this->db->limit(HOME_ARTICLE_LIMIT, $offset);
            $res = $this->db->get('articles')->result();            
            if (!empty($res)) {
                return $res;
            } else {
                return [];
            }
        }
    }

    function get_article_details($slug) {
        $this->db->select('a.*,CONCAT(u.firstname," ",u.surname) as added_by');
        $this->db->from('articles as a');
        $this->db->join('admin_users as u', 'u.user_id = a.created_by', 'inner');
        $this->db->where('a.article_slug', $slug);
        // $this->db->group_by('p.product_id');
        $row = $this->db->get()->row();
        if (!empty($row)) {
            return $row;
        } else {
            return [];
        }
    }

}
