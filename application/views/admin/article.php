<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Article
            <small>Listing</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/article'); ?>"><i class="fa fa-bars"></i>Article</a></li>
            <li class="active">Listing</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Article Table</h3>
                        <p align="right">
                            <a href="<?php echo base_url('admin/article'); ?>" class="btn btn-success btn-flat btn-md"><i class="fa fa-refresh"></i> Reset</a>
                            <a href="<?php echo base_url('admin/article/save'); ?>" class="btn btn-primary btn-flat btn-md"><i class="fa fa-plus"></i> Add Article</a>
                        </p>
                    </div>
                    <div class="box-body">
                        <table id="article_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Image Preview</th>
                                    <th>Article Category</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    var base_url = '<?php echo base_url(); ?>';
    $(function () {
        $('#article_table').DataTable({
            "paging": true,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "columnDefs": [{orderable: false,targets: [0,1,2,3,4,5]}],
            "info": true,
            "lengthMenu": [10, 20, 30, 50],
            "autoWidth": true,
            "ajax": "<?php echo base_url('admin/article/paginate'); ?>",
            language: {
                searchPlaceholder: "Search by title"
            }
        });
    });

    function changeArticleStatus(elem,id,status){
        console.log(elem);
        $.ajax({
            url: '<?php echo base_url('admin/article/change_status'); ?>',
            type: 'post',
            data: {
                'article_id' : id,
                'status' : status
            },
            dataType:'json',
            beforeSend: toggle_overlay(true),
            success: function (resp) {
                toggle_overlay(false);
                if(resp.status == 1){
                    notify('Status Changed Successfully ...!!!', 'success', 'bottom', 'right');
                    $(elem.closest('td')).html(resp.html);
                }else{
                    notify('Failed to update status ...!!!', 'danger', 'bottom', 'right');
                }
            }
        });
    }

</script>