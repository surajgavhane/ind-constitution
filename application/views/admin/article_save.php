<style>
    .select2-selection__rendered{
        margin-top:2px;
    }
    .select2-container--default .select2-selection--single{
        height:38px !important;
        border-radius: 0px !important;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Article
            <small>
                <?php
if ($edit_id) {
    echo 'Edit';
} else {
    echo 'Add';
}
?>
            </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/article'); ?>"><i class="fa fa-bars"></i>Article</a></li>
            <li class="active">
                <?php
if ($edit_id) {
    echo 'Edit';
} else {
    echo 'Add';
}
?>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <p align="right">
                            <a href="<?php echo base_url('admin/article'); ?>" class="btn btn-primary btn-flat btn-md"><i class="fa fa-backward"></i> back</a>
                        </p>
                    </div>
                    <?php
if ($edit_id) {
    $action = base_url('admin/article/save/' . $edit_id);
} else {
    $action = base_url('admin/article/save');
}
?>
<hr>
                    <form id="frm_article" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="col-md-9">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Article Basic Details </h3>
                                    </div>
                                    <div class="form-group">
                                        <label>Select Article Category<label class="error1">*</label></label>
                                        <select name="category_id" class="required form-control select2">
                                            <option value="">Select Article Category</option>
                                            <?php foreach ($all_categories as $category) { ?>
                                                    <option value="<?php echo $category->category_id; ?>" <?php
if (set_value('category_id') == $category->category_id) {
    echo 'selected="selected"';
} elseif (isset($article_details->category_id)) {
    if ($article_details->category_id == $category->category_id) {
        echo 'selected="selected"';
    }
}
    ?>><?php echo $category->category_name; ?>
                                                    </option>
                                                <?php } ?>
                                        </select>
                                        <?php if (count($all_categories) == 0) { ?><label class="error">Please add categories first</label><?php } ?>
                                        <label class="error"><?php echo form_error('category_id'); ?></label>
                                    </div>
                                    <div class="form-group">
                                        <label>Images<label class="error1"></label></label>                                  
                                        <input class="form-control" id="product_images" name="image" type="file" placeholder="Add images" accept="image/gif, image/jpeg,image/jpg, image/png"  />
                                        <br />
                                        <label class="error"><?php echo form_error('image'); ?></label>
                                    </div> 
                                    <div class="form-group">
                                        <label>Article Title<label class="error1">*</label></label>
                                        <input class="form-control" name="article_title" type="text" placeholder="Article Title" value="<?php
if (set_value('article_title') != "") {
    echo set_value('article_title');
} elseif (isset($article_details->article_title)) {
    echo $article_details->article_title;
}
?>" />
                                        <label class="error"><?php echo form_error('article_title'); ?></label>
                                    </div>
                                    <div class="form-group">
                                        <label>Article Body<label class="error1">*</label></label>
                                        <textarea class="form-control" name="article_body" placeholder="Article Body" rows="5"><?php
if (set_value('article_body') != "") {
    echo set_value('article_body');
} elseif (isset($article_details->article_body)) {
    echo $article_details->article_body;
}
?></textarea>
                                        <label class="error"><?php echo form_error('article_body'); ?></label>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success btn-flat btn-md">Save</button>
                                    <a href="<?php echo base_url('admin/article'); ?>" class="btn btn-danger btn-flat btn-md">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';

</script>
<script src="<?php echo base_url('public/admin'); ?>/custom/jquery.validate.min.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/custom/additional-methods.min.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/plugins/select2/select2.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/custom/admin_article.js"></script>

