<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="<?php echo get_active_class('dashboard', $tab); ?>">
                <a href="<?php echo base_url('admin/dashboard'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="<?php echo get_active_class('article', $tab); ?>">
            <a href="<?php echo base_url('admin/article'); ?>">
                    <i class="fa fa-bars"></i> <span>Articles</span>
                </a>

            </li>
            <li class="treeview <?php echo get_active_class('site_setting', $tab); ?> <?php echo get_active_class('payment_setting', $tab); ?> ">
                <a href="javascript:void(0);">
                    <i class="fa fa-bars"></i> <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu <?php echo get_menu_class('site_setting', $tab); ?>">
                    <li class="<?php echo get_active_class('site_setting', $tab); ?>">
                        <a href="<?php echo base_url('admin/setting/site'); ?>">
                            <i class="fa fa-bars"></i>
                            <span>Site Setting</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </section>
</aside>