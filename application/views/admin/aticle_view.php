<style>

    table>tbody > tr > td,table>thead>tr>th {
        text-align:left !important;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Product
            <small>Listing</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/product'); ?>"><i class="fa fa-bars"></i>Product</a></li>
            <li class="active">Listing</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Product Details: <b><?php echo $product_details->name; ?></b></h3>
                        <p align="right">
                            <a href="<?php echo base_url('admin/product'); ?>" class="btn btn-primary btn-flat btn-md"><i class="fa fa-backward"></i> back</a>
                        </p>
                    </div>
                    <div class="box-body">    
                        <div class="preview-images-zone" id="product_image_container">
                            <?php
                            $i = 1;
                            foreach ($product_images as $pr_image) {
                                $pr_img_url = get_product_url($pr_image->name, $pr_image->product_id);
                                ?>
                                <div class="preview-image preview-show-<?php echo $i; ?> <?php
                                if ($pr_image->is_primary == 1) {
                                    echo 'preview-primary';
                                }
                                ?>">
                                    <div class="image-zone"><img id="pro-img-<?php echo $i; ?>" src="<?php echo $pr_img_url; ?>"></div>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                        <div class="col-md-6">
                            <div class="box-header with-border">
                                <h3 class="box-title">Product Basic Details </h3>
                            </div>
                            <table  class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td><?php echo $product_details->name; ?></td>
                                    </tr> 
                                    <tr>
                                        <td>Description</td>
                                        <td><?php echo $product_details->description; ?></td>
                                    </tr> 
                                    <tr>
                                        <td>Price</td>
                                        <td><?php echo currency() . $product_details->price; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Discount</td>
                                        <td><?php echo ($product_details->discount) ? $product_details->discount : 'N.A.'; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Discount Type</td>
                                        <td><?php echo ($product_details->discount_type) ? $product_details->discount_type : 'N.A.'; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Stock In Hand</td>
                                        <td><?php echo $product_details->current_stock; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Featured</td>
                                        <td><?php echo ($product_details->is_featured) ? 'Yes' : 'No'; ?></td>
                                    </tr> 
                                    <tr>
                                        <td>Pre Booking</td>
                                        <td><?php echo ($product_details->pre_booking) ? 'Yes' : 'No'; ?></td>
                                    </tr> 
                                    <tr>
                                        <td>Status</td>
                                        <td><?php echo ($product_details->status) ? 'Active' : 'Inactive'; ?></td>
                                    </tr> 
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <div class="box-header with-border">
                                <h3 class="box-title">Product Other Details </h3>
                            </div>
                            <table  class="table table-bordered table-hover">
                                <thead>
                                <th>Attribute Name</th>
                                <th>Attribute Value</th>                                
                                </thead>
                                <tbody>
                                    <tr>  
                                        <td>Available In Color</td>
                                        <td>
                                            <?php
                                            foreach ($product_colors as $color) {
                                                echo '<div  class="color-preview" style="float:left;background-color:' . $color->color_code . '"></div>';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td>Available In Sizes</td>
                                        <td>
                                            <?php
                                            $sizes = '';
                                            foreach ($product_sizes as $size) {
                                                $sizes .= $size->size . ',';
                                            }
                                            echo rtrim($sizes, ',');
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                    foreach ($product_attributes as $pr_attr) {
                                        ?>
                                        <tr>  
                                            <td><?php echo $pr_attr->attribute_name ?> </td>
                                            <td><?php echo $pr_attr->attribute_value; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>