<div class="modal fade in" id="change_password">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <form id="frm_change_password" method="post" action="<?php echo base_url('login/change_password') ?>" >

                    <div class="form-group">
                        <label>Old Password<label class="error1">*</label></label>
                        <input class="form-control required" name="old" type="password" value="" />
                    </div>
                    <br>
                    <div class="form-group">
                        <label>New Password<label class="error1">*</label></label>
                        <input class="form-control required" name="password1" type="password" value="" />
                    </div>
                    <br>
                    <div class="form-group">
                        <label>Confirm Password<label class="error1">*</label></label>
                        <input class="form-control required" name="password2" type="password" value="" />
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button id="save_password" type="button" class="btn btn-primary">Save</button>
                <button id="cancel_password" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="full-screen-overlay" class="full-screen-overlay" style="display:none;"></div>
<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="javascript:void(0);"><?php echo _session('site_copyright'); ?></a>.</strong> All rights
    reserved.
</footer>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
</script>
<script src="<?php echo base_url('public/admin'); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/plugins/fastclick/fastclick.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/dist/js/app.min.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/dist/js/demo.js"></script>
<script src="<?php echo base_url('public/admin'); ?>/custom/bootstrap-notify.min.js"></script>        
<script src="<?php echo base_url('public/admin'); ?>/custom/common_functions.js"></script>        
<script src="<?php echo base_url('public/admin'); ?>/custom/custom.js"></script>        
<script>
<?php
$_msg = $this->session->flashdata('msg');
if (!empty($_msg)) {
    ?>
        notify('<?php echo $_msg ?>', 'success', 'bottom', 'right');
<?php }
?>
<?php
$_error = $this->session->flashdata('error');
if (!empty($_error)) {
    ?>
        notify('<?php echo $_error ?>', 'danger', 'bottom', 'right');
<?php }
?>
</script>
</body>
</html>
