<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">        
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?php echo _session('site_name'); ?></title>
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/plugins/iCheck/flat/blue.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/plugins/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/plugins/select2/select2.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/custom/custom_style.css">
        <script src="<?php echo base_url('public/admin'); ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <script src="<?php echo base_url('public/admin'); ?>/custom/jquery-ui.min.js"></script>                
        <script src="<?php echo base_url('public/admin'); ?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url('public/admin'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url('public/admin'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo base_url('public/admin'); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>

        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <a href="<?php echo base_url('admin/dashboard'); ?>" class="logo">
                    <!--<span class="logo-mini"><b>SL</b></span>-->
                    <span class="logo-lg">
                        <img src="<?php echo _session('site_logo'); ?>" style="height:50px;width:200px;"/>
                    </span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="javasript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs"><?php echo _session('fullname'); ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <p>
                                            <?php echo _session('fullname'); ?>
                                            <small><?php echo _session('email'); ?></small>
                                        </p>
                                        <br>
                                        <div class="pull-left">
                                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#change_password">Change Password</button>
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo base_url('admin/login/logout'); ?>" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <?php $this->load->view('admin/aside'); ?>