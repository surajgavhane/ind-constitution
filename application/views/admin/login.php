<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log in</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/dist/css/AdminLTE.min.css">        
        <link rel="stylesheet" href="<?php echo base_url('public/admin'); ?>/custom/custom_style.css">        
    </head>
    <body class="hold-transition login-page">
        <div class="login-box" >
            <div class="login-logo">
                <a href="javascript:void(0)"><b>Login</b></a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>
                <label class="error">
                    <?php
                    if (!empty($error)) {
                        echo $error;
                    }
                    ?>
                </label>
                <form id="frm_admin_lodin" action="<?php echo base_url('admin/login'); ?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Email" name="username">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <label class="error"><?php echo form_error('username'); ?></label>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name ="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <label class="error"><?php echo form_error('password'); ?></label>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <script src="<?php echo base_url('public/admin'); ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>                
    </body>
</html>
