<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Site Settings <small>Listing</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('admin/setting/site'); ?>"><i class="fa fa-bars"></i>Site Settings</a></li>
            <li class="active">Listing</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <p align="right">
                            <a href="<?php echo base_url('admin/setting/site'); ?>" class="btn btn-success btn-flat btn-md"><i class="fa fa-refresh"></i> Reset</a>
                        </p>
                    </div>
                    <hr />
                    <div class="box-body">
                        <table id="setting_table" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sr.No.</th>
                                    <th>Name</th>
                                    <th>Value</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade in" id="setting_modal_logo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Change Logo</h4>
            </div>
            <div class="modal-body">
                <form id="frm_setting_logo" method="post" action="<?php echo base_url('admin/setting/save_logo') ?>" enctype="multipart/form-data">
                    <input type="hidden" id="setting_edit_id_logo" name="edit_id" value="" />
                    <div class="form-group">
                        <label>Images<label class="error1">*</label></label>
                        <br>
                        <span id="trigger_upload" class="btn btn-success btn-flat btn-md">Upload Logo</span>                                        
                        <input class="form-control" id="product_images" name="image" type="file" placeholder="Add images" accept="image/gif, image/jpeg,image/jpg, image/png"  style="display:none;"/>                                        
                        <br />
                        <label class="error"><?php echo form_error('images'); ?></label>
                    </div>
                    <div class="preview-images-zone" id="product_image_container">
                        <div class="preview-image">
                            <div class="image-zone"><img id="pro-img-" src=""></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="save_setting_logo" type="button" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="setting_add_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Add Setting</h4>
            </div>
            <div class="modal-body">
                <form id="frm_setting_save" method="post" action="<?php echo base_url('admin/setting/site_save') ?>" >
                    <input type="hidden" id="setting_edit_id" name="edit_id" value="" />
                    <div class="form-group">
                        <label id="label_setting_name"><label class="error1">*</label></label>
                        <textarea class="form-control required" id="input_setting" name="setting_value" cols="4"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="save_setting" type="button" class="btn btn-primary">Save</button>
                <button id="cancel_setting_form" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#setting_table').DataTable({
            "paging": true,
            "searching": true,
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "columnDefs": [{orderable: false, targets: [0, 2, 3]}],
            "info": true,
            "lengthMenu": [20, 30, 50, 100],
            "autoWidth": true,
            "ajax": "<?php echo base_url('admin/setting/site_paginate'); ?>",
            language: {
                searchPlaceholder: "Search Name Or Value"
            }
        });


        $('#save_setting').click(function () {
            if ($('#input_setting').val() == '') {
                set_error($('#input_setting'), 'Please enter some value');
            } else {
                $('#frm_setting_save').submit();
            }
        });

        $(document).on('click', '.setting-edit-open', function () {
            $('#setting_edit_id').val($(this).attr('data-sid'));
            $('#input_setting').val($(this).attr('data-sval'));
            $('#label_setting_name').html($(this).attr('data-sname'));
            $('#setting_add_modal').modal('show');
        });
    });

</script>