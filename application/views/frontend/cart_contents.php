<?php
$_cart_total = 0;
$_cart_subtotal = 0;
$_cart_delivery = 0;
$_cart_discount = 0;
$cart_contents = _session('cart');
?>
<div id="cart_contents">
    <div class="cart-content d-flex">
        <div class="cart-list">
            <?php
            if (count($cart_contents) > 0) {
                foreach ($cart_contents as $cart) {
                    ?>

                    <div class="single-cart-item">
                        <a href="javascript:void(0);" class="product-image">
                            <img src="<?php echo $cart['image']; ?>" class="cart-thumb" alt="">
                            <div class="cart-item-desc">
                                <span class="product-remove btn-cart-remove cart-div" data-pid="<?php echo $cart['product_id']; ?>"><i class="fa fa-close" aria-hidden="true"></i></span>
                                <h6><?php echo $cart['name']; ?></h6>
                                <p class="size">Size: <?php echo $cart['size_name']; ?></p>
                                <p class="color">Color: <?php echo $cart['color_name']; ?></p>
                                <p class="price"><?php echo currency() . $cart['price']; ?></p>
                            </div>
                        </a>
                    </div>

                    <?php
                    $_cart_subtotal = $cart['price'] + $_cart_subtotal;
                    $_cart_total = $cart['price'] + $_cart_total;
                }
            }
            ?>
        </div>
        <div class="cart-amount-summary">
            <?php if (count($cart_contents) > 0) { ?>
                <h2>Summary</h2>
            <?php } else {
                ?>
                <h2>Empty Cart</h2>
            <?php }
            ?>
            <ul class="summary-table">
                <li><span>subtotal:</span> <span><?php echo currency() . $_cart_subtotal; ?></span></li>
                <li><span>delivery:</span> <span><?php echo currency() . $_cart_delivery; ?></span></li>
                <li><span>discount:</span> <span><?php echo currency() . $_cart_discount; ?></span></li>
                <li><span>total:</span> <span><?php echo currency() . $_cart_total; ?></span></li>
            </ul>
            <div class="checkout-btn mt-100">
                <?php if (_session('_logged_in') == 'yes') { ?>
                    <a href="<?php echo base_url('checkout'); ?>" class="btn essence-btn">check out</a>
                <?php } else {
                    ?>
                    <a href="<?php echo base_url('user/login?redirect=checkout'); ?>" class="btn essence-btn">check out</a>
                <?php } ?>
            </div>
        </div>
    </div>

</div>