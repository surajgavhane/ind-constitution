<div class = "breadcumb_area bg-img" style = "background-image: url(<?php echo base_url('public/frontend/'); ?>img/bg-img/breadcumb.jpg);">
    <div class = "container h-100">
        <div class = "row h-100 align-items-center">
            <div class = "col-12">
                <div class = "page-title text-center">
                    <h2>Checkout</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="checkout_area section-padding-80">
    <div class="container">
        <div class="row">
            <?php
            $_cart_total = 0;
            $_cart_subtotal = 0;
            $_cart_delivery = 0;
            $_cart_discount = 0;
            $cart_contents = _session('cart');
            ?>
            <table class="table table-bordered">
                <thead>
                <th>Image</th>
                <th>Product</th>
                <th class="text-center">Price</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Subtotal</th>
                </thead>
                <tbody>
                    <?php
                    if (count($cart_contents) > 0) {
                        foreach ($cart_contents as $cart) {
                            ?>
                            <tr>
                                <td style="width:12%;">
                                    <a href="javascript:void(0);" class="product-image">
                                        <img src="<?php echo $cart['image']; ?>" class="cart-thumb" alt="" style="width:100%;height:auto;">
                                    </a>
                                </td>
                                <td>
                                    <div class="cart-item-desc">
                                        <p>
                                            <?php echo $cart['name']; ?><br>
                                            Size: <?php echo $cart['size_name']; ?><br>
                                            Color: <?php echo $cart['color_name']; ?><br>
                                            <a href="<?php echo base_url('home/remove_cart/') . $cart['product_id']; ?>" style="color:blue;">Remove</a>
                                        </p>
                                    </div>
                                </td>
                                <td class="text-center"><?php echo $cart['price']; ?></td>
                                <td class="text-center">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn update-quantity minus"  data-type="minus" data-field="quantity[<?php echo $cart['product_id']; ?>]">
                                                <span class="fa fa-minus"></span>
                                            </button>
                                        </span>
                                        <input type="number" name="quantity[<?php echo $cart['product_id']; ?>]" class="form-control input-number" value="<?php echo $cart['quantity']; ?>" min="1" max="100" style="width:20px">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn update-quantity" data-type="plus" data-field="quantity[<?php echo $cart['product_id']; ?>]">
                                                <span class="fa fa-plus"></span>
                                            </button>
                                        </span>
                                    </div>

                                </td>
                                <td class="text-center"><?php echo $cart['price'] * 1; ?></td>
                            </tr>
                            <?php
                            $_cart_subtotal = $cart['price'] + $_cart_subtotal;
                            $_cart_total = $cart['price'] + $_cart_total;
                        }
                    } else {
                        ?>
                        <tr><td colspan="5" class="text-center">Cart is empty...!!!</td></tr>
                    <?php }
                    ?>
                </tbody>
            </table>
        </div>
        <?php if (count($cart_contents) > 0) { ?>
            <div class="row">
                <div class="col-12 col-md-6 order-details-confirmation">
                    <div class="checkout_details_area clearfix ">
                        <div class="cart-page-heading mb-30">
                            <h5>Shipping Address</h5>
                        </div>
                        <form action="checkout" method="post">
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label for="street_address">Address <span>*</span></label>
                                    <textarea type="text" class="form-control mb-3" id="street_address" value=""></textarea>
                                </div>                           
                                <div class="col-12 mb-3">
                                    <label for="city">Town/City <span>*</span></label>
                                    <input type="text" class="form-control" id="city" value="">
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="city">District<span>*</span></label>
                                    <input type="text" class="form-control" id="district" value="">
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="state">State <span>*</span></label>
                                    <input type="text" class="form-control" id="state" value="">
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="postcode">Postal Code/Zip Code <span>*</span></label>
                                    <input type="number" min="0" class="form-control" id="postcode" value="">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                    <div class="order-details-confirmation">
                        <div class="cart-page-heading">
                            <h5>Your Order</h5>
                            <p>The Details</p>
                        </div>
                        <ul class="order-details-form mb-4">
                            <li><span>Subtotal</span> <span><?php echo currency() . $_cart_subtotal; ?></span></li>
                            <li><span>Shipping</span> <span><?php echo currency() . $_cart_delivery; ?></span></li>
                            <li><span>Discount</span> <span><?php echo currency() . $_cart_discount; ?></span></li>                        
                            <li><span>Total</span> <span><?php echo currency() . $_cart_total; ?></span></li>
                        </ul>  

                        <div class="card">
                            <div class="card-body">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra tempor so dales. Phasellus sagittis auctor gravida. Integ er bibendum sodales arcu id te mpus. Ut consectetur lacus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div clas="row">
                <div class="order-details-confirmation">                   
                    <div id="accordion" role="tablist" class="mb-4">
                        <div class="card">
                            <div class="card-header" role="tab" id="headingOne">
                                <h6 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><i class="fa fa-circle-o mr-3"></i>Paypal</a>
                                </h6>
                            </div>

                            <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin pharetra tempor so dales. Phasellus sagittis auctor gravida. Integ er bibendum sodales arcu id te mpus. Ut consectetur lacus.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingTwo">
                                <h6 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-circle-o mr-3"></i>cash on delievery</a>
                                </h6>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quis in veritatis officia inventore, tempore provident dignissimos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingThree">
                                <h6 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fa fa-circle-o mr-3"></i>credit card</a>
                                </h6>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse quo sint repudiandae suscipit ab soluta delectus voluptate, vero vitae</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" role="tab" id="headingFour">
                                <h6 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"><i class="fa fa-circle-o mr-3"></i>direct bank transfer</a>
                                </h6>
                            </div>
                            <div id="collapseFour" class="collapse show" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est cum autem eveniet saepe fugit, impedit magni.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="btn essence-btn">Proceed</a>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script>
    $('.update-quantity').click(function (e) {
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');

        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {
                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
            } else if (type == 'plus') {
                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function () {
        $(this).data('oldValue', $(this).val());
    });

    $('.input-number').change(function () {
        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            notify('Sorry, the minimum value was reached', 'danger', 'bottom', 'right');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            notify('Sorry, the maximum value was reached', 'danger', 'bottom', 'right');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                // Allow: Ctrl+A
                        (e.keyCode == 65 && e.ctrlKey === true) ||
                        // Allow: home, end, left, right
                                (e.keyCode >= 35 && e.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
</script>