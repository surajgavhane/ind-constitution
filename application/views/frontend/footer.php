<footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>

  </footer>
<div id="full-screen-overlay" class="full-screen-overlay" style="display:none;"></div>
<script>
<?php
$_msg = $this->session->flashdata('_message');
if (!empty($_msg)) {
    ?>
        notify('<?php echo $_msg ?>', 'success', 'bottom', 'right');
<?php }
?>
<?php
$_error = $this->session->flashdata('_error');
if (!empty($_error)) {
    ?>
        notify('<?php echo $_error ?>', 'danger', 'bottom', 'right');
<?php }
?>
    var _base_url = '<?php echo base_url(); ?>';
    var slider_prev_img = _base_url + 'public/frontend/img/core-img/long-arrow-left.svg';
    var slider_next_img = _base_url + 'public/frontend/img/core-img/long-arrow-right.svg';
</script>
<script src="<?php echo base_url('public/frontend/'); ?>js/popper.min.js"></script>
<script src="<?php echo base_url('public/frontend/'); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url('public/frontend/'); ?>js/plugins.js"></script>
<script src="<?php echo base_url('public/frontend/'); ?>js/classy-nav.min.js"></script>
<script src="<?php echo base_url('public/frontend/'); ?>js/active.js"></script>
<script src="<?php echo base_url('public/frontend/'); ?>js/notify.js"></script>


</body>

</html>