<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo _session('_site_name'); ?></title>
        <link rel="icon" href="<?php echo base_url('public/frontend/'); ?>img/core-img/favicon.ico">
        <link rel="stylesheet" href="<?php echo base_url('public/article/'); ?>vendor/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url('public/frontend/'); ?>css/custom_style.css">
        <script src="<?php echo base_url('public/frontend/'); ?>js/jquery/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url('public/frontend/'); ?>js/bootstrap-notify.min.js"></script>
        <script src="<?php echo base_url('public/frontend/'); ?>js/custom.js"></script>
    </head>
    <body>
    
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container">
            <a class="nav-brand" href="<?php echo base_url('home'); ?>" >
                <img src="<?php echo _session('_site_logo'); ?>" alt="" class="site-logo">
            </a>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            <?php if (_session('_logged_in') == 'yes') { ?>
                <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo base_url('home');?>">Home
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo base_url('logout');?>">Logout</a>
                </li>
                </ul>
                <?php } else { ?> 
                    <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo base_url('login');?>">Login
                        </a>
                    </li>
                    </ul>
                <?php } ?>
            </div>
            </div>
        </nav>
    
