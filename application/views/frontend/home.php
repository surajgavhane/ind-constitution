<link href="<?php echo base_url('public/article/'); ?>css/heroic-features.css" rel="stylesheet">
<div class="container">
<header class="jumbotron my-4">
  <h1 class="display-3">A Warm Welcome!</h1>
  <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, ipsam, eligendi, in quo sunt possimus non incidunt odit vero aliquid similique quaerat nam nobis illo aspernatur vitae fugiat numquam repellat.</p>
  <a href="#" class="btn btn-primary btn-lg">Call to action!</a>
</header>

<?php if(!empty($recent_articles)) { ?>
<div class="row text-center">
<?php foreach($recent_articles as $ra) { ?>
  <div class="col-lg-3 col-md-6 mb-4">
    <div class="card h-100">
      <img class="card-img-top" src="<?php echo get_article_url($ra->article_image);?>" alt="" style="height:150px;width:auto;">
      <div class="card-body">
        <h4 class="card-title"><?php echo truncate_string($ra->article_title,50);?></h4>
        <p class="card-text"><?php echo truncate_string($ra->article_body,100);?></p>
      </div>
      <div class="card-footer">
        <a href="<?php echo base_url('view/'.$ra->article_slug);?>" class="btn btn-primary">Read More!</a>
      </div>
    </div>
  </div>
<?php }?>
</div>
<?php } ?>
</div>
