<div class="checkout_area">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                <div class="order-details-confirmation">
                    <div class="cart-page-heading">
                        <h5>Register</h5>
                        <p>Create an account</p>
                    </div>
                    <form id="frm_register" action="<?php echo base_url('user/register'); ?>" method="post">
                        <div class="row">
                           <div class="col-12 mb-4">
                                <label for="first_name">First Name <span class="require-span">*</span></label>
                                <input type="text" class="form-control required" name="first_name" value="<?php echo set_value('first_name');?>" placeholder="First Name" />
                                <span class="required-alert"><?php echo form_error('first_name');?></span>
                            </div>
                            <div class="col-12 mb-4">
                                <label for="last_name">Last Name <span class="require-span">*</span></label>
                                <input type="text" class="form-control required" name="last_name" value="<?php echo set_value('last_name');?>" placeholder="Last Name" />
                                <span class="required-alert"><?php echo form_error('last_name');?></span>
                            </div>
                            <div class="col-12 mb-4">
                                <label for="email_address">Email Address <span class="require-span">*</span></label>
                                <input type="email" class="form-control required" name="email" value="<?php echo set_value('email');?>" placeholder="Email" />
                                <span class="required-alert"><?php echo form_error('email');?></span>
                            </div>
                            <div class="col-12 mb-3">
                                <label for="phone_number">Mobile Number <span class="require-span">*</span></label>
                                <input type="text" class="form-control required mobile" name="contact_number" id="contact_number" placeholder="Mobile Number" value="<?php echo set_value('contact_number');?>" />
                                <span class="required-alert"><?php echo form_error('contact_number');?></span>
                            </div>
                            <div class="col-12 mb-4">
                                <label for="password1">Password<span class="require-span">*</span></label>
                                <input type="password" class="form-control required" name="password1" value="" placeholder="Password" />
                                <span class="required-alert"><?php echo form_error('password1');?></span>
                            </div>
                            <div class="col-12 mb-4">
                                <label for="password2">Comfirm Password<span class="require-span">*</span></label>
                                <input type="password" class="form-control required" name="password2" value="" placeholder="Confirm Password" />
                                <span class="required-alert"><?php echo form_error('password2');?></span>
                            </div>
                            <div class="col-12">
                                <!-- <div class="custom-control custom-checkbox d-block mb-2">
                                    <input type="checkbox" class="custom-control-input required" id="customCheck1" name="agree" value="1">
                                    <label class="custom-control-label" for="customCheck1">Terms and conitions</label>
                                </div> -->
                                <a id="do_register" href="javascript:void(0);" class="btn essence-btn">Login</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5 ml-lg-auto">
                <div class="order-details-confirmation">
                    <div class="cart-page-heading">
                        <h5>Login</h5>
                        <p>Already have an account?</p>
                    </div>
                    <div id="accordion" role="tablist" class="mb-4">
                        <form id="frm_login" action="<?php echo base_url('user/login'); ?>" method="post">
                            <div class="row">
                                
                                </span>
                                <div class="col-12 mb-4">
                                    <label for="email_address">Email/Username <span class="require-span">*</span></label>
                                    <input type="text" name="username" class="form-control required" id="username" value="" />
                                    <span class="required-alert"><?php echo form_error('username');?></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label for="password">Password<span class="require-span">*</span></label>
                                    <input type="password" class="form-control required" id="password" value="" name="password" />
                                    <span class="required-alert"><?php echo form_error('password');?></span>
                                </div>
                                <div class="col-12">
                                    <span class="required-alert">
                                        <?php if(isset($error)){
                                            echo $error;
                                        }
                                        ?>
                                    </span>
                                    <br>
                                    <a href="javascript:void(0);" id="do_login" class="btn essence-btn">Login</a>
                                    <!-- <a href="do_forget_password" class="btn essence-btn" style="display: none;">Submit</a> -->
                                    <!-- <a href="javascript:void(0);" id="btn_forget_password" style="display: none;">Login</a> -->
                                    <!-- <a href="javascript:void(0);" id="btn_forget_password">Forgot Password</a> -->
                                </div>
                            </div>
                        </form>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>