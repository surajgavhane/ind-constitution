<div class="col-12 col-md-4 col-lg-3" style="border-right: 1px solid #c1c1c1;">
    <div class="shop_sidebar_area">
        <div class="widget catagory mb-50">
            <h6 class="widget-title mb-30">Catagories</h6>
            <div class="catagories-menu">
                <ul id="menu-content2" class="menu-content collapse show">
                    <?php foreach ($sidebar_categories as $category) { ?>
                        <li data-toggle="collapse" data-target="#<?php echo $category['url_slug']; ?>">
                            <a href="javascript:void(0);"><?php echo $category['category_name']; ?></a>
                            <ul class="sub-menu collapse <?php
                                if ($filter['category'] === $category['url_slug']) {
                                    echo 'show';
                                }
                                ?>" id="<?php echo $category['url_slug']; ?>">
                                    <?php foreach ($category['sub_categories'] as $sub_category) { ?>
                                    <li><a href = "<?php
                                        if (isset($_GET['search'])) {
                                            echo base_url('collection/search') . '?search=' . $_GET['search'] . '&type=' . $category['url_slug'] . '&sub_type=' . $sub_category['url_slug'];
                                        } else {
                                            echo base_url('collection') . '?type=' . $category['url_slug'] . '&sub_type=' . $sub_category['url_slug'];
                                        }
                                        ?>"><?php echo $sub_category['sub_category_name']; ?></a></li>
                                    <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>