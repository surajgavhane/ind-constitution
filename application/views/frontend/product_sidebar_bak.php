<div class="col-12 col-md-4 col-lg-3">
    <div class="shop_sidebar_area">
        <div class="widget catagory mb-50">
            <h6 class="widget-title mb-30">Catagories</h6>
            <div class="catagories-menu">
                <ul id="menu-content2" class="menu-content collapse show">
                    <?php foreach ($sidebar_categories as $category) { ?>
                        <li data-toggle="collapse" data-target="#<?php echo $category['url_slug']; ?>">
                            <a href="javascript:void(0);"><?php echo $category['category_name']; ?></a>
                            <ul class="sub-menu collapse <?php
                            if ($filter['category'] === $category['url_slug']) {
                                echo 'show';
                            }
                            ?>" id="<?php echo $category['url_slug']; ?>">
                                    <?php foreach ($category['sub_categories'] as $sub_category) { ?>
                                    <li><a href = "<?php
                                        if (isset($_GET['search'])) {
                                            echo base_url('collection/search') . '?search=' . $_GET['search'] . '&type=' . $category['url_slug'] . '&sub_type=' . $sub_category['url_slug'];
                                        } else {
                                            echo base_url('collection') . '?type=' . $category['url_slug'] . '&sub_type=' . $sub_category['url_slug'];
                                        }
                                        ?>"><?php echo $sub_category['sub_category_name']; ?></a></li>
                                    <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>    
        <?php if (!empty($sidebar_colors)) { ?>
            <div class="widget color mb-50">
                <p class="widget-title2 mb-30">Color</p>
                <div class="widget-desc">
                    <ul class="d-flex">
                        <?php foreach ($sidebar_colors as $color) { ?>
                            <li>
                                <a href="javascript:void(0);" title="<?php echo $color->color_name; ?>" style="background-color: <?php echo $color->color_code; ?>" class="<?php
                                if ($filter['color_id'] == $color->color_id) {
                                    echo 'selected';
                                }
                                ?>"></a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
        <div class="widget price mb-50">
            <!-- Widget Title -->
            <h6 class="widget-title mb-30">Filter by</h6>
            <!-- Widget Title 2 -->
            <p class="widget-title2 mb-30">Price</p>

            <div class="widget-desc">
                <div class="slider-range">
                    <div data-min="<?php echo $min_price; ?>" data-max="<?php echo $max_price; ?>" data-unit="<?php echo currency(); ?>" class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" data-value-min="49" data-value-max="360" data-label-result="Range:">
                        <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                        <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
                        <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
                    </div>
                    <div class="range-price">Range: <?php
                        echo currency() . $min_price . ' - ' . currency() . $max_price;
                        ?>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                        <a href="javascript:void(0)" class="product-price-range pull-right btn btn-sm btn-primary" >Go</a>
                    </div>
                </div>
            </div>
        </div>
        <!--        <div class = "widget brands mb-50">
                    <p class = "widget-title2 mb-30">Brands</p>
                    <div class = "widget-desc">
                        <ul>
                            <li><a href = "#">Asos</a></li>
                            <li><a href = "#">Mango</a></li>
                            <li><a href = "#">River Island</a></li>
                            <li><a href = "#">Topshop</a></li>
                            <li><a href = "#">Zara</a></li>
                        </ul>
                    </div>
                </div>-->
    </div>
</div>