<section class="new_arrivals_area clearfix" style="margin-top: 20px;">
    <div class="container">
        <div class="section-heading text-center">
            <h2><?php echo $slider_title; ?></h2>                        
        </div>
        <div class="popular-products-slides owl-carousel">
            <?php foreach ($products as $product) { ?>
                <div class="single-product-wrapper">
                    <!-- Product Image -->
                    <div class="product-img">
                        <img src="<?php echo get_product_url($product->image_name, $product->product_id) ?>" alt="" style="height:275px;width:100%;">
                    </div>
                    <!-- Product Description -->
                    <div class="product-description">
                        <a href="<?php echo base_url('collection/view') . '?name=' . $product->url_slug; ?>">
                            <h6><?php echo $product->name; ?></h6>
                        </a>
                        <p class="product-price"><?php echo currency() . $product->price; ?></p>
                        <?php
                        if ($product->discount) {
                            ?>
                            <span> 
                                <?php
                                if ($product->discount_type == '1') {
                                    echo currency() . $product->discount;
                                } else {
                                    echo $product->discount . '%';
                                }
                                ?> Discount
                            </span>
                        <?php } ?>
                        <div class="hover-content">
                            <div class="add-to-cart-btn">
                                <a href="<?php echo base_url('collection/view') . '?name=' . $product->url_slug; ?>" class="btn essence-btn">View</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="text-center"><a href="<?php echo $view_all_url; ?>" style="font-size:18px;">View All</a></div>
    </div>
</section>