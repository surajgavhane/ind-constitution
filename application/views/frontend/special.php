<div class = "breadcumb_area bg-img" style = "background-image: url(<?php echo base_url('public/frontend/'); ?>img/bg-img/breadcumb.jpg);">
    <div class = "container h-100">
        <div class = "row h-100 align-items-center">
            <div class = "col-12">
                <div class = "page-title text-center">
                    <h2>Results For: <?php echo $filter['special_type']; ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="shop_grid_area section-padding-80">
    <div class="container">
        <div class="row">
            <?php $this->load->view('frontend/product_sidebar'); ?>
            <div class="col-12 col-md-8 col-lg-9">
                <div class="shop_grid_product_area">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-topbar d-flex align-items-center justify-content-between">
                                <div class="total-products">
                                    <p>Showing <span><?php echo $total_count; ?></span> search result<?php if ($total_count > 1) { ?>s<?php } ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <div class="single-product-wrapper">
                                    <div class="product-img">
                                        <img src="<?php echo get_product_url($product->image_name, $product->product_id); ?>" alt="">                                       
                                        <?php if ($product->discount) { ?>
                                            <div class="product-badge offer-badge">
                                                <span>
                                                    <?php
                                                    if ($product->discount_type == 1) {
                                                        echo currency() . $product->discount;
                                                    } else {
                                                        echo $product->discount . '%';
                                                    }
                                                    ?>&nbsp; Discount
                                                </span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="product-description">
                                        <a href="<?php echo base_url('collection/view') . '?name=' . $product->url_slug; ?>">
                                            <h6><?php echo $product->name; ?></h6>
                                        </a>
                                        <?php if ($product->discount) { ?>
                                            <p class="product-price"><span class="old-price"><?php echo currency() . $product->price; ?></span><?php echo currency() . get_duscount_price($product->price, $product->discount, $product->discount_type); ?></p>
                                        <?php } else { ?>
                                            <p class="product-price"><?php echo currency() . $product->price; ?></p>
                                        <?php } ?>
                                        <div class="hover-content">
                                            <div class="add-to-cart-btn">
                                                <a href="<?php echo base_url('collection/view') . '?name=' . $product->url_slug; ?>" class="btn essence-btn">View</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php
                $_base = base_url('collection/') . $query_string;
                echo create_pagination($total_pages, $page_offset, $total_count, $_base);
                ?>
            </div>
        </div>
    </div>
</section>