
$(document).ready(function () {
    $(".select2").select2();

    $("#frm_article").validate({
        rules: {
            article_title: {
                required: true,
            },
            article_body: {
                required: true,
            },
            category_id: {
                required: true,
            },
        },
        messages: {
            article_title: {
                required: "This field is required",
            },
            article_body: {
                required: "This field is required",
            },
            category_id: {
                required: "This field is required",
            },
        },
    });

});