function confirm_delete(url, msg) {
    if (!msg) {
        msg = 'Are you sure want to delete ?';
    }
    var _html = '<div class="modal fade" id="modal-confirm-delete"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">' + msg + '</h4></div><form action="' + url + '" method="post"><div class="modal-footer"><button type = "button" class ="btn btn-default" data-dismiss="modal">No</button><button type="button" class="btn btn-primary confirm_delete">Yes</button></form></div></div></div></div>';
    $(_html).modal('show');

}
function validate_number(elem, min, max) {
    var must_num = 'The field must contains only numbers';
    var len = 'The field has invalid length of numbers';
    var num = elem.val();
    var regex = /[0-9 -()+]+$/;
    if (!regex.test(num)) {
        set_error(elem, must_num);
        return false;
    } else if (!(num.length <= max && num.length >= min)) {
        set_error(elem, len);
        return false;
    } else {
        clear_error(elem);
        return true;
    }
}

function set_error(elem, error) {
    var span = '<span class="require_alert" style="float:left;">' + error + '</span>';
//    elem.css({'border': '1px solid red'});
    if (!elem.closest('div').find('.require_alert').length) {
        elem.closest('div').append(span);
    }

}

function clear_error(elem) {
//    elem.css({'border': '1px solid #ccc'});
    elem.closest('div').find('.require_alert').remove();
}
function validate_fields(id) {
    var valid = true;
    var req = 'Required';

    var invalid_email = 'Invalid Email Address';
    $('#' + id).find('.required').each(function () {
        var here = $(this);
        if (here.val() == '') {
            set_error(here, req);
            valid = false;
        } else if (here.attr('type') == 'tel') {
            if (!validate_number(here, 10, 10)) {
                valid = false;
            }
        } else if (here.attr('type') == 'zip') {
            if (!validate_number(here, 4, 6)) {
                valid = false;
            }
        } else if (here.attr('type') == 'email') {
            if (!isValidEmailAddress(here.val())) {
                set_error(here, invalid_email);
                valid = false;
            } else {
                clear_error(here);
            }
        } else {
            clear_error(here);
        }
    });

    return valid;
}

function notify(message, type, from, align) {
    $.notify({
        message: message
    }, {
        type: type,
        placement: {
            from: from,
            align: align
        }
    });
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function toggle_overlay(flag) {
    if (flag) {
        $('#full-screen-overlay').show();
    } else {
        $('#full-screen-overlay').hide();
    }
}
function printDiv(divName, title) {
    var print_title = 'Print';
    if (title) {
        print_title = title;
    }
    var printContents = '<html><head><title>' + print_title + '</title><link rel="stylesheet" href="' + base_url + 'public/bootstrap/css/bootstrap.min.css" /></head><body onload="window.print()">';
    printContents = printContents + document.getElementById(divName).innerHTML;
    printContents = printContents + '</body></html>';
    var popupWin = window.open();
    popupWin.document.open()
    popupWin.document.write(printContents);
    popupWin.document.close();
    popupWin.focus();
}