$(document).on('keyup keydown change', '.required', function () {
    clear_error($(this));
});

$(document).on('click', '.confirm_delete', function () {
    $(this).closest('form').submit();
});

$('#save_password').click(function () {
    var valid = validate_fields('frm_change_password');
    if (valid) {
        $.ajax({
            url: $('#frm_change_password').attr('action'),
            type: 'post',
            data: $('#frm_change_password').serialize(),
            dataType:'json',
            beforeSend: toggle_overlay(true),
            success: function (resp) {
                toggle_overlay(false);
                if (resp.status == 'done') {
                    notify('Password Changed Successfully ...!!!', 'success', 'bottom', 'right');
                    $('#cancel_password').click();
                } else if (resp.status == 'invalid') {
                    notify('Invalid Old Password ..!!!', 'danger', 'bottom', 'right');
                } else if (resp.status == 'not_matching') {
                    notify('Both Password Are Not Matching ...!!!', 'danger', 'bottom', 'right');
                } else if(resp.status == 'validate'){
                    notify('All Fields Are Required ..!!!', 'danger', 'bottom', 'right');
                }
            }
        });
    }
});

$('#cancel_password').click(function () {
    $('#frm_change_password').find('input').each(function () {
        $(this).val('');
    });
});