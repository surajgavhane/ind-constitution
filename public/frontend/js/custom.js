function cart_refresh() {
    $.ajax({
        url: _base_url + 'home/cart/refresh',
        type: 'post',
        dataType: 'html',
        success: function (_html) {
            $('#cart_contents').html(_html);
        }
    });
}
function confirm_delete(url, msg) {
    if (!msg) {
        msg = 'Are you sure want to delete ?';
    }
    var _html = '<div class="modal fade" id="modal-confirm-delete"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">' + msg + '</h4></div><form action="' + url + '" method="post"><div class="modal-footer"><button type = "button" class ="btn btn-default" data-dismiss="modal">No</button><button type="button" class="btn btn-primary confirm_delete">Yes</button></form></div></div></div></div>';
    $(_html).modal('show');

}
function validate_number(elem, min, max) {
    var must_num = 'Please type only numbers';
    var len = 'Invalid mobile number';
    var num = elem.val();
    var regex = /[0-9 -()+]+$/;
    if (!regex.test(num)) {
        set_error(elem, must_num);
        return false;
    } else if (!(num.length <= max && num.length >= min)) {
        set_error(elem, len);
        return false;
    } else {
        clear_error(elem);
        return true;
    }
}

function set_error(elem, error) {
    clear_error(elem);
    var span = '<span class="required-alert" style="float:left;color:#ff084e;">' + error + '</span>';
    elem.closest('div').append(span);
}

function clear_error(elem) {
    elem.closest('div').find('.required-alert').remove();
}
function validate_fields(id) {
    var valid = true;
    var req = 'Required';

    var invalid_email = 'Invalid Email Address';
    $('#' + id).find('.required').each(function () {
        var here = $(this);
        if (here.val() == '') {
            set_error(here, req);
            valid = false;
        } else if (here.hasClass('mobile')) {
            if (!validate_number(here, 10, 10)) {
                valid = false;
            }
        } else if (here.attr('type') == 'zip') {
            if (!validate_number(here, 4, 6)) {
                valid = false;
            }
        } else if (here.attr('type') == 'email') {
            if (!isValidEmailAddress(here.val())) {
                set_error(here, invalid_email);
                valid = false;
            } else {
                clear_error(here);
            }
        } else {
            clear_error(here);
        }
    });

    return valid;
}

function notify(message, type, from, align) {
    $.notify({
        message: message
    }, {
        type: type,
        placement: {
            from: from,
            align: align
        }
    });
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-+\s]+")|([\w-+]+(?:\.[\w-+]+)*)|("[\w-+\s]+")([\w-+]+(?:\.[\w-+]+)*))(@((?:[\w-+]+\.)*\w[\w-+]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][\d]\.|1[\d]{2}\.|[\d]{1,2}\.))((25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\.){2}(25[0-5]|2[0-4][\d]|1[\d]{2}|[\d]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function loader(flag) {
    if (flag) {
        $('#full-screen-overlay').show();
    } else {
        $('#full-screen-overlay').hide();
    }
}
function printDiv(divName, title) {
    var print_title = 'Print';
    if (title) {
        print_title = title;
    }
    var printContents = '<html><head><title>' + print_title + '</title><link rel="stylesheet" href="' + base_url + 'public/bootstrap/css/bootstrap.min.css" /></head><body onload="window.print()">';
    printContents = printContents + document.getElementById(divName).innerHTML;
    printContents = printContents + '</body></html>';
    var popupWin = window.open();
    popupWin.document.open()
    popupWin.document.write(printContents);
    popupWin.document.close();
    popupWin.focus();
}

(function ($) {
    $(document).on('focus', '.required', function () {
        clear_error($(this));
    });

    $(document).on('click', '#do_login', function () {
        var valid = validate_fields('frm_login');
        if (valid) {
            $('#frm_login').submit();
        }
    });
    $(document).on('click', '#do_register', function () {
        var valid = validate_fields('frm_register');
        if (valid) {
            $('#frm_register').submit();
        }
    });

    $(document).on('click', '.btn-cart', function () {
        var that = $(this);
        var product_id = $(this).attr('data-pid');
        $.ajax({
            url: _base_url + 'home/cart/add/' + product_id + '/' + $('#productSize').val() + '/' + $('#productColor').val(),
            type: 'post',
            dataType: 'json',
            beforeSend: loader(true),
            success: function (resp) {
                loader(false);
                if (resp.status == 'added') {
                    that.html('Remove from cart').addClass('btn-cart-remove').removeClass('btn-cart');
                    var cnt = parseInt($('#cart_count_span').html());
                    cnt++;
                    $('#cart_count_span').html(cnt);
                    notify(resp.message, 'success', 'bottom', 'right');
                } else if (resp.status == 'exists') {
                    that.html('Remove from cart').addClass('btn-cart-remove').removeClass('btn-cart');
                    notify(resp.message, 'danger', 'bottom', 'right');
                } else if (resp.status == 'expired') {
                    notify(resp.message, 'danger', 'bottom', 'right');
                } else {

                }
                cart_refresh();
            }
        });
    });
    $(document).on('click', '.btn-cart-remove', function () {
        var product_id = $(this).attr('data-pid');
        var that = $(this);
        $.ajax({
            url: _base_url + 'home/cart/remove/' + product_id,
            type: 'post',
            dataType: 'json',
            beforeSend: loader(true),
            success: function (resp) {
                loader(false);
                if (resp.status == 'ok') {
                    var cnt = parseInt($('#cart_count_span').html());
                    cnt--;
                    $('#cart_count_span').html(cnt);
                    if (that.hasClass('cart-div')) {
                        $(document).find('.btn-cart-remove[data-pid="' + product_id + '"]').html('Add To cart').removeClass('btn-cart-remove').addClass('btn-cart');
                    } else {
                        that.html('Add To cart').removeClass('btn-cart-remove').addClass('btn-cart');
                    }
                    notify(resp.message, 'success', 'bottom', 'right');
                }

                cart_refresh();
            }
        });
    });

})(jQuery);